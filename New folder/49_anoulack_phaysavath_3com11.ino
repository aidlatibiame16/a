const int relay = 8;

void setup()
{
  Serial.begin(115200);
  pinMode(relay, OUTPUT);
}

void loop()
{
  digitalWrite(relay, HIGH);
  delay(1000); // Wait for 1000 millisecond(s)
  digitalWrite(relay, LOW);
  delay(1000); // Wait for 1000 millisecond(s)
}
